第12章  经销商
滴滴滴——

隔着老远，俞淮玲便看到自家厂子方向聚集了不少人，似乎都围观什么，汽车鸣笛声也时不时从人群尽头传来。

她立刻反应过来，一定是运送机器的车子到了，心中不由得一阵喜悦，连忙把腿往前跑去。

果不其然，跑到近处才看到人群中央停着一辆送货的汽车，司机似乎已经等了不少时间了，有些着急，不停地按着喇叭。

人群中，有人认出俞淮玲，急忙对司机喊起来：“好了好了，人来了。”

司机当即打开车门跳下来，一脸不悦地打开后车门，嘴上嘟嘟囔囔的：“怎么这么慢，我都等了快半个小时了。”

“不好意思不好意思。”

俞淮玲看到机器眼睛都亮了，道歉便也显得不那么走心，司机撇了她一眼，继续嘟囔：“我只是送货的，并不负责卸货，你自己想想办法，有没有认识的人可以帮你把这些机器搬下去。”

“我来吧。”

人群中有男人的声音才砸出来，后头便立刻跟了一道女人低声斥骂的声音：“侬发哪门子神经，人家办这个厂是来同你抢生意的，你还要去帮人家的，是不是脑子进水了？”

这声音落在所有人耳里，原本热闹围观的气氛瞬间变了，所有人都默契地收起好奇打量的目光，摆摆手散了开去：“也没什么好看的，回家喝酒去。”

司机没想到围观的人会一哄而散，他着急地看着手表，有些头疼地看着这些机器：“那你说怎么办？”

俞淮玲当即转身对着逐渐走远的人群大喊道：“帮我搬一下机器，十块钱一个人，就要四个人。”

这话一喊出去，那些个疾步往前走的人几乎是在瞬间转身，小跑着冲到俞淮玲面前。

俞淮玲看着站在面前的十来位男女老少，不好意思道：“只要中年男子，王大伯您还是算了吧。”

王大伯听到这话不干了，悻悻开口：“这么几台机器，再重能用到哪里去？我这个力气上山打老虎都吃得消勒，你别看不起我！”

“大伯，我不是看不起你，是这地上湿湿滑滑的，不好走，万一摔一跤，那就不是十块钱能弥补的了，也不划算。”

俞淮玲这边还迂回着，司机已经顾自开始指挥了：“你你你，你，还有你，就你们几个了，快点上来搬吧，砸碎可是要赔钱的。”

司机只点了三个人，自己当做第四人上了，吭哧吭哧地将机器往仓库里搬去。

俞淮玲看着这三台八成新的机器，简直要乐开了花，忍不住上手东摸摸西摸摸，直到司机向她要钱，她才回过神来，忙将搬运的费用结了。

万事俱备，只差伞骨架了，这事好办，她直接在张老板给的小本中选了一个价格最为便宜的模具厂，打了电话过去。

电话那头的人听说是张老板那边过来的，态度立刻爽快起来。

“是的是的，老张我知道，虽然我和他合作的时间不长，但是他这个人我是非常喜欢的，人特别爽快，特别对我胃口，我们一直合作的也都非常愉快，你是他那边过来的，肯定也不差。”

俞淮玲听了也乐呵呵的，可下一秒还是神色凝重着，小心翼翼追问道：“那价格方面……”

“按理说，老张什么价格，我就该给你什么价格，但是小姑娘啊，你不知道，最近这个钢铁都涨价了，我们这个成本上去了，实在是不可能给出原先的价钱了。”

听到这话，俞淮玲心里一凉，担心这价格过高会让自己赔本，果不其然，当对方报出价格后，俞淮玲粗略一算，不赚反赔。

“这个价格有点高了，您这边可以……”

话音未落，电话那头的人爽快着带着不耐烦打断了她：“小姑娘，我们都是爽快人，你又是从老张那边过来的，我是绝对不会坑你的，现在市场上就是这样的价，没办法，你不相信可以去任何一家厂问，绝对不会有比我们的价格出的更低的。我这也是看在老张的面子，才给你这个价的，其他家来我们这拿价，绝不可能是这个价，而且我们家都是现货，你要的话明天就让货车发出去。”

对方说的斩钉截铁又爱答不理，言语之中显然不缺俞淮玲这一单生意，俞淮玲犹豫之下，决定将雨伞的销售价格提高，于是点头应了：“好，那就这个价，感谢感谢。”

解决完伞骨后，俞淮玲又买了些零碎的其他需要的东西，等她收拾完，天早就黑了，而她却没有回家的意思，拿着纸笔不停地写写算算，试图算出利益最大化。

咚咚咚——

敲门声猝不及防地传来，俞淮玲抬眼看去，只见两个看上去有些熟悉的身影，正局促不安地站在门口，似乎是在犹豫什么，并不肯迈进来。

“二位阿姨，有什么事吗？”

俞淮玲放下手中的笔，起身走向她们，她们侧头打量了彼此一眼，然后默契地点点头，这才侧头看向俞淮玲，紧张开口：“我们是来应聘的，你白天说的话还算不算数？”

“是你们呀！”俞淮玲这才认出来，原来这两位就是白天打麻绳的阿姨之一。

她走到两人跟前，伸手指了指门边的椅子，示意边坐边说：“当然算数的，不过这两天东西还没到齐，你们可以回家准备一下，到时候再过来。”

两位阿姨并没有坐下的意思，而是面露喜色，一个劲地点头：“行行行，我们是桥头那边的，我叫张纷，她叫叶萍，要是能上班了，麻烦你来跟我们说一声。”

“好的，张阿姨，叶阿姨。”

俞淮玲笑着点点头，目送她们离开厂子，心里却依旧盘算着利润。

她记得张老板的小本本上所记载的一些价格，大部分的经销商都是以三块三的价格收入，最贵的是时候则是三块八。

可不管怎么算，想要在短时间内赚到大量的钱都显得有些困难，唯有从质量上来取胜了。

她走到电话旁，开始给经销商们一个个的打电话。

“您好，我这边是张老板介绍过来的太阳伞厂，不知您最近是否有采购的需要？”

电话那头的人闻言连连噢噢了两声：“你说的是老张吧，本来和他合作很愉快的，自从听说他家里出事后，我还没来得及去找别的雨伞厂收货，如果你们厂里能有货的话也是很好，老张介绍的人我信得过。”

“那真是太好了，冒昧请问您这边采购价格是……”

俞淮玲问出这话，有些紧张地将话筒贴近耳朵，一颗心几乎提到了嗓子眼。

“啊，我这边价格现在是三块一。”

听到这个回答，俞淮玲十分意外，吃惊道：“三块一？这有些低了，我这边这个价的话亏得不是一点两点……”

不等她说完，电话那头的人也有些无奈地解释：“没办法呀，现在伞多了，市场价格被压下来了，我要是收的价格高了，下面的商店超市不好卖啊。”

俞淮玲没想到市场发展的这样快，心里略微有些失落，这个价格她是万万不能接受的，只得抱歉着开口：“对不住啊，这个价格我是真的不行，以后如果还有机会，期待能和您合作。”

“好的。”

就在俞淮玲准备挂电话的时候，电话那头的人又补了一句：“不过小妹妹，我提醒你一句哈，现在的市场价都差不多是这个价位，你去问别的经销商也是差不多的，你得从源头上解决这个问题，比如把伞布和伞骨的价格压下去，他们现在订单多了，价格也是时候压下去一点了。”

“谢谢您告诉我这些。”

俞淮玲挂完电话后，心头乱乱的，她无力地坐在椅子上，一双眉头紧紧地蹙着。

经销商收货后将这些货物送到一些知名商店，有时候也会送到批发市场，这样一来，那些商店和批发市场的摊主拿到的价格必然要远远高于经销商的收购价。

如果自己把经销商的这项工作包揽了，利润岂不是平白高出好几个比例？

想到这里，俞淮玲一双眼睛又亮了起来，只是具体要怎么操作，她还没有想好，一想到到时候机器开了，三个人要干六个人的活，根本没有时间去想别的事，俞淮玲便觉得身心疲惫。

不过从目前的情况来讲，她也确实招不起更多的人了。

“玲玲，哇，你什么时候买的机器，这动作也太快了吧！”

熟悉的声音从门口传来，俞淮玲笑着抬眼看去，见王薇一脸好奇地东瞧瞧西瞧瞧，便跟着她身旁一台接一台的介绍着。

“不错不错，真不错，玲玲，你果然就和我想的一样厉害！”王薇说着露出一个得意的笑容，斜眼道，“我就说我的眼光是绝对不可能出现问题的，你瞧瞧，干得多漂亮？”

俞淮玲闻言苦笑一声，摇头道：“你就别挖苦我了，我现在是分身乏术，想要包揽经销商的活，却抽不开身。”

“这有什么，你姐妹不是人吗？这种工作交给我不就行了，说得好听是经销商，其实不就是个推货的吗，只要我们比其他经销商的价格压得低，怎么会推不出去？”

俞淮玲见王薇大有入伙的意思，不免吃惊追问：“你不是学的会计吗，之前听你说你已经面试了一家非常不错的大公司啊。”

“那有什么，和你一起为家乡做奋斗才是我最开心的事。”王薇见仓库里除了机器并没有什么库存，便假模假样地催促起来，“玲玲，不是我说你，你这做伞的速度可要加紧啊，不能我到时候找到了一堆可以收伞的地方，你的雨伞却还没搞出来。”

王薇这话逗得俞淮玲哈哈大笑起来，也学着她假模假样的样子，恭敬点头道：“好的，老板，我一定努力做伞，不让你失望，当然，工资也要记得加啊。”

“哈哈哈哈哈。”

两人同时大笑起来，俞淮玲沉闷的情绪在这欢快的气氛中一扫而光。

