第11章  招工不易
绍兴上虞当下虽然伞厂不多，可纺织工业却是极其的发达，小本本上虽然有着几家不错的纺织厂，可是和本地的纺织厂比起来价格还是高了一截，更何况，小本本上那几家纺织厂都在杭州，来去不便，不如直接在当地选购。

俞淮玲一大早地便去了俞术读那儿，将昨天的好消息告诉他之后，又向他请教了附近便宜的纺织厂，巧的是，俞术读岳父的表舅家是开纺织厂的，俞淮玲拿了联系方式直奔这家鸿伟纺织厂。

鸿伟纺织厂如今的老板是俞术读岳父表舅的小孙子陈飞跃，年纪很轻，才三十出头，能力却不容小觑。

俞淮玲从俞术读那听说这位陈飞跃，读完高中后死活不肯上大学，非要进家里的厂子做事，家里人为了阻拦他，故意让他从头做起，结果人家根本不在意，就是要从最累最脏最基础的活开始做，

这一做就是整整十年，整个人厂的细节和方向都牢牢把控在他手中，从上到下，没有一个人不服他，他家里人便也彻底放手，将厂子交给了他。

陈飞跃听说俞书记介绍了人过来买布匹，十分地重视，早早便在门卫处等着了，当初他年纪轻轻就很有主见，却遭到了所有反对，除了当时的俞书记还能说几句宽慰的话，其他人没给过他好脸色看，更是没在他身上放几分希望，一心觉得他不求上进，宁可混吃混喝也不愿去上学。

如今他成功了，扬眉吐气了，记在心里的永远是俞书记当初的安慰和鼓励，而不是现在身旁人的各种夸赞。

所以当他看到同样年轻的俞淮玲出现在眼前的时候，瞬间便明白了俞术读的用意，一双眼睛不免有些热泪盈眶。

他从俞淮玲的眼睛里，好像看到了十年前那个愣头青一样的自己，固执又坚定，勇敢又鲁莽。

“陈老板您好……”

俞淮玲一开口，便是那样的客套和死板，陈飞跃听了忍不住扑哧笑出声来：“要不要这么正经，我有那么老吗，你叫我飞跃哥就好了。”

“好的，飞跃哥。”

俞淮玲不敢和面前的开玩笑，依旧是一本正经的模样，不等她说出自己此行的用意，陈飞跃便抢先开口了：“我听俞书记说你买布匹是用来做伞的吧？”

他见俞淮玲点点头，便啧了一声诧异道：“我记得我们上虞并没有什么雨伞厂啊，你是要做二道贩子，赚差价吗？”

俞淮玲轻声一笑，摇头解释：“不是的，是我开了一家伞厂，是上虞的第一家伞厂。”

“厉害厉害，年纪轻轻，真有勇气。”陈飞跃说到这里，见俞淮玲还是一本正经的模样，忍不住眉飞色舞，故意打趣起来，“有我当年的风采。”

俞淮玲闻言登时噗嗤一声笑出来，察觉不妥，又忙捂嘴：“飞跃哥，您真会开玩笑。”

话音未落，俞淮玲又提起正事：“飞跃哥，不知道厂里能做雨伞的布料有哪些？”

“外面有哪些布料我不知道，我们这只有涤纶的，防水还是可以的，你要看的话，跟我到这边来。”

陈飞跃说到正事立刻收起了脸上的嬉笑，一脸认真地带着俞淮玲往车间走去，他介绍道：“毕竟我们这边是没有什么雨伞厂的，来下订单的，也就是附近几个城市，订单量不是很多，所以我们没有特意拉出一条制作雨伞的生产线，一般都是要提前预订，我们再拉出生产线赶出来，如果几家雨伞厂撞上了，那就只能拖一拖了，或者他们实在着急先去找别的厂子救急。”

“那一般要多久？”

一听说要等，俞淮玲的小心脏就颤抖起来，陈飞跃却不以为意道：“也就几天功夫吧，还是挺快的，你离得近就更方便了，先做一匹出来带回去，剩下的边做边带。”

这倒是个不错的主意，俞淮玲当即点头询问价格：“那这个布料是什么价格？”

“两块一码，一匹布大概是200码，长柄伞能做150把，短柄伞大概能做250把。”

俞淮玲大脑飞速旋转，她并没有做长柄伞的打算，短柄伞相较于长柄伞更好卖更容易携带，一把短柄伞如果定价4元销售的话，那就是1000块钱，而布匹成本价在600块钱，那就是每销售250把，能赚400块。

可除去这些，还要加上工人的工钱，招工不够的话，一天也做不完250把，这样算下来利润就很薄了。

俞淮玲有些不好意思地笑着开口：“飞跃哥，能不能再便宜一些，这个价格我有些难。”

陈飞跃闻言也爽快，直言道：“别的厂在我这边都是这个价格的，你是自己这边人，又是俞书记介绍来的，我就给你便宜点，一块九……”

“一块七吧。”

俞淮玲虽然不好意思，可嘴上却极快地吐出一个数字，实在和不好意思这四个字没有任何关系。

陈飞跃听到这个数字，有些哭笑不得：“妹妹啊，这个价格，我也很难做的，我家厂子主要还是做衣服不料的，雨伞这边真的赚不到什么钱，我知道你刚起步，什么都很难，但也不要让我太亏了，一块八好吗，不要再讲价了。”

“好的！谢谢飞跃哥！”

能还一毛钱对俞淮玲而言也非常非常不错了，当下忙对陈飞跃连连道谢，看过料子后，俞淮玲当场交了十匹布的定金。

在回太阳街的路上，俞淮玲的目光在马路边逗留许久，马路边有三四个中年妇女，她们正一边说笑着闲聊一边手脚麻利地整理着麻绳。

一团团的麻绳被码的整整齐齐，看上去是相当的舒适，俞淮玲知道这麻绳是按件算钱的闲工，一般一团麻绳也没多少钱，从早到晚不停地干也比不过在厂里上一天的班。

不过如今厂不好上，她们又要顾家，能做的也就是这样的工作了。

俞淮玲越看这几位阿姨越顺眼，尤其是这麻溜的动作，如果能招进自己的厂，岂不是事半功倍？

她没忍住，抬脚走向几人：“阿姨，你们做这个一天能赚多少钱啊？”

几人听到俞淮玲这直接开口的问话，也没感到冒犯，毫不掩饰道：“没多少钱，但是每天的饭钱还是有的。”

俞淮玲闻言当即邀请道：“阿姨，我在前面几百米的地方有个厂，是刚开的，一天20块钱，做得多可以再加钱。”

四个人听到这话有些不可思议，十分默契地放下手中的麻绳，异口同声地追问道：“什么？多少钱？！”

“一天20块钱，做得多还可以再加钱。”

俞淮玲见她们眼睛都亮了起来，心里也乐开了花，觉得这招工应该十分的顺畅。

其中一个看上去较为精明的女人开口追问道：“真的假的哦，你个小姑娘不要骗阿姨们啊，你那是什么厂，做什么的？”

“是伞厂，做雨伞的，操作非常简单，教一遍就会了。”

这话一出口，面前几个中年妇女的脸色当即一变，眼中的光登时消散的干干净净。

看上去较为精明的女人当即摇头摆手，重新拿起麻绳：“不去不去。”

她这一开口，旁边几个人也沉默起来，纷纷重新拿起麻绳，俞淮玲见状不免着急：“为什么呀，很近的，时间也很宽松，如果你们要临时回来照顾一下家里也是可以的，工作也不辛苦的。”

另一个看上去较为瘦削，眼眶有些凹陷的女人摇摇头，开口点醒：“我们几个的老公都是做伞的，你这工厂要是开起来，他们靠什么赚钱去？”

“可是就算没有我，也会有别人来开厂，也就是我们这里没有伞厂，外面那些地方不都已经有伞厂了吗？机器伞的确是质量次一点，但它也有轻巧灵便容易携带的好处，在市场非常的流行……”

俞淮玲喋喋不休地说着，可是面前几位都不愿意再听她继续说下去，俞淮玲只得祭出大杀器：“如果完成当日份额的话，可以额外奖励五块钱，完成当月份额，可以额外奖励一百块钱，完成季度份额，可以额外奖励四百块钱，完成年度份额，可以额外奖励一千五百块钱。”

这一句句的糖衣炮弹，对这些阿姨而言，实在是天大的诱惑，手中的麻绳顿时没了干劲，可饶是如此，大家左右打量着彼此的脸色，还是没人开口。

俞淮玲微微叹了口气，最后道：“各位阿姨，我的厂就在前面，很近的，如果你们想来工作的话，随时来找我，不过……”

说到这里，俞淮玲故意一顿，等到所有人将目光落在自己身上，才卖关子似的补充：“我的厂子也没那么大，如果来的太迟的话，可能人数就够了，不招了，阿姨们要是诚心想来，记得早点来。”

说完这话，俞淮玲才转身离开，这一路上始终走走停停，搜寻着合适的人选，可看来看去，还是更喜欢那四位绑麻绳的阿姨，手脚麻溜又整齐，最是适合做伞了。

