from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

    def setupUi(self, MainWindow):
        self.centralwidget = MainWindow
        self.centralwidget.resize(960, 720)
        self.centralwidget.setWindowTitle("小说下载器")

        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        top = (screen.height() - size.height()) / 2 - 40
        left = (screen.width() - size.width()) / 2
        self.move(left, top)

        # self.label_1 = QLabel(self.centralwidget)
        # self.label_1.setGeometry(20, 20, 120, 40)
        # self.label_1.setText("请选择一个小说源")
        #
        # self.combobox_sources = QComboBox(self.centralwidget)
        # self.combobox_sources.setGeometry(150, 20, 200, 40)
        # self.combobox_sources.addItem("https://www.81zw.com")

        # 700, 20, 200, 40
        self.lineEdit_search = QLineEdit(self.centralwidget)
        self.lineEdit_search.setPlaceholderText("请输入小说名字")
        self.lineEdit_search.setGeometry(20, 20, 400, 40)

        self.btn_search = QPushButton(self.centralwidget)
        self.btn_search.setText("搜索")
        self.btn_search.setGeometry(500, 20, 120, 40)
        self.btn_search.setIcon(QIcon("UI/search.svg"))

        self.btn_load = QPushButton(self.centralwidget)
        self.btn_load.setText("从本地加载")
        self.btn_load.setGeometry(650, 20, 120, 40)

        self.btn_suggest = QPushButton(self.centralwidget)
        self.btn_suggest.setText("推荐")
        self.btn_suggest.setGeometry(800, 20, 120, 40)

        self.table_results = QTableWidget(self.centralwidget)
        self.table_results.setWindowTitle("搜索结果")
        self.table_results.setGeometry(20, 80, 920, 400)
        self.table_results.setColumnCount(5)
        self.table_results.verticalHeader().setVisible(False)

        self.textbrowser_download = QTextBrowser(self.centralwidget)
        self.textbrowser_download.setGeometry(20, 500, 920, 180)

        self.progressbar_1 = QProgressBar(self.centralwidget)
        self.progressbar_2 = QProgressBar(self.centralwidget)
        self.progressbar_3 = QProgressBar(self.centralwidget)
        self.progressbar_4 = QProgressBar(self.centralwidget)

        self.progressbar_1.setGeometry(20, 690, 200, 20)
        self.progressbar_2.setGeometry(260, 690, 200, 20)
        self.progressbar_3.setGeometry(500, 690, 200, 20)
        self.progressbar_4.setGeometry(740, 690, 200, 20)

