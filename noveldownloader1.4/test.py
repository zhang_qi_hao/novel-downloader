#!/usr/bin/env python
# coding=utf-8

def str_union(s1, s2):
    union_parts = []
    for i in range(1, len(s1)+1):
        if i <= len(s2):
            if s1[-i:] == s2[:i]:
                union_parts.append(s2[:i])
    if union_parts == []:
        return s1 + s2
    else:
        return s1.replace(union_parts[-1], "") + s2


s1 = 'https://www.lewen.cc/94/94468/'
s2 = '/94/94468/39301200.html'
union = str_union(s1, s2)
print(union)
