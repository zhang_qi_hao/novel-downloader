import re


def read_ua(path):
	with open(path, 'r') as f:
		page = f.read()
	pattern = re.compile(r'"(.*?)"')
	uas = pattern.findall(page)
	return uas
