import re
import random
import urllib
import requests
import readua
from lxml import html



uas = readua.read_ua('UA.txt')

def get_html(url):

    headers = {
        'User-Agent': random.choice(uas)
    }
    # print(headers)
    try:
        r = requests.get(url, headers=headers)
        r.raise_for_status()
        return r.text
    except:
        return ''


def get_books(surl):
    page = get_html(surl)
    pattern = re.compile(r'<a href="(/book/\d+/)">(\w+)</a>')
    bs = pattern.findall(page)
    books = []
    for item in bs:
        c = surl + item[0]
        books.append({
            'link': c,
            'title': item[1]
        })
   
    return books


def get_chapters(burl, bookname):
    page = get_html(burl)
    pattern = re.compile(r'<dd><a href="/book/\d+/(\d+\.html)"  >(第.*?)</a></dd>')
    cs = pattern.findall(page)
    chapters = []
    for item in cs:
        c = burl + item[0]
        chapters.append({
            'link': c,
            'title': item[1],
            'bookname': bookname
        })
    # print(chapters)
    return chapters


def get_content(curl):
    page = get_html(curl)
    pattern = re.compile(r'<div id="content">(.*?)</div>')
    content = pattern.findall(page)[0].replace('<br>', '\n').replace('&nbsp;', '')
    return content

def search(keyword):
    qurl = "https://www.81zw.com/search.php?q={}".format(urllib.parse.quote_plus(keyword))
    surl = 'https://www.81zw.com'
    page = get_html(qurl)
    # print(url)
    homepage = html.fromstring(page)
    results = homepage.xpath('/html/body/div[3]')[0]
    burl = results.xpath('//h3/a/@href')
    burls = [surl+url for url in burl]
    titles = results.xpath('//h3/a/span/text()')
    authors = results.xpath('//div/p[1]/span[2]/text()')
    novel_types = results.xpath('//div/p[2]/span[2]/text()')
    update_times = results.xpath('//div/p[3]/span[2]/text()')
    # books = zip(titles, burls, authors, novel_types, update_times)
    books = []
    for i in range(len(titles)):
        book = {
            'title': str(titles[i]),
            'burl': burls[i],
            'author': authors[i],
            'novel_type': novel_types[i],
            'update_time': update_times[i]
        }
        books.append(book)
    return books

if __name__ == '__main__':
    # burl = 'https://www.81zw.com/book/10150/'
    # surl = 'https://www.81zw.com'
    # books = get_books(surl)
    # print(books)
    r = search("元尊")
    # for item in r:
    #     print(item)
    # print(len(r))
    # while True:
    #     # print(r.__next__())
    #     print(r.__sizeof__())
   

