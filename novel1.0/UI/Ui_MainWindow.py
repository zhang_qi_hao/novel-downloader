from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

    def setupUi(self, MainWindow):
        # 主窗口的大小和标题
        self.centralwidget = MainWindow
        self.centralwidget.resize(600, 150)
        self.centralwidget.setWindowTitle("App")

        # 窗口居中
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        top = (screen.height() - size.height()) / 2 - 40
        left = (screen.width() - size.width()) / 2
        self.move(left, top)
        # print(left, top)

        self.stackw_1 = QStackedWidget(self.centralwidget)
        self.stackw_1.setGeometry(QRect(0, 0, 600, 300))
        self.page_1 = QWidget()
        self.page_2 = QWidget()
        self.page_3 = QWidget()

        self.stackw_1.addWidget(self.page_1)
        self.stackw_1.addWidget(self.page_2)
        self.stackw_1.addWidget(self.page_3)

        # page_1
        self.label1 = QLabel(self.page_1)
        self.label1.setText("小说源")
        self.label1.setGeometry(QRect(10, 20, 80, 20))
        self.combobox_surl = QComboBox(self.page_1)
        self.combobox_surl.addItem('https://www.81zw.com')
        self.combobox_surl.setGeometry(QRect(100, 20, 200, 20))
        # combobox.setCurrentIndex(0)
        self.btn_refresh = QPushButton(self.page_1)
        self.btn_refresh.setText("刷新")
        self.btn_refresh.setGeometry(QRect(400, 20, 80, 20))
        self.book_list = QListWidget(self.page_1)
        self.book_list.setGeometry(QRect(10, 80, 500, 300))

        # page_2
        self.label2 = QLabel(self.page_2)

        # self.label2.setGeometry(QRect(570, 40, 200, 40))
        self.label2.setFont(QFont('微软雅黑', 18))

        self.chapter_list = QListWidget(self.page_2)
        # self.chapter_list.setGeometry(QRect(0, 100, 200, 500))
        self.chapter_list.resize(400, 500)

        vlayout2 = QVBoxLayout(self.page_2)
        vlayout2.addWidget(self.label2)
        vlayout2.addWidget(self.chapter_list)

        hlayout2 = QHBoxLayout()
        self.btn_jump = QPushButton()
        self.btn_jump.setText("跳转")

        self.btn_locate_page_1 = QPushButton()
        self.btn_locate_page_1.setText("回到首页")

        self.btn_download_all = QPushButton()
        self.btn_download_all.setText("下载所有")

        self.lineEdit_jump = QLineEdit()
        self.label3 = QLabel()
        self.label4 = QLabel()
        hlayout2.addWidget(self.lineEdit_jump)
        spacer = QSpacerItem(300, 40)
        hlayout2.addWidget(self.label3)
        hlayout2.addSpacerItem(spacer)
        hlayout2.addWidget(self.label4)
        hlayout2.addWidget(self.btn_download_all)
        hlayout2.addWidget(self.btn_jump)
        hlayout2.addWidget(self.btn_locate_page_1)

        vlayout2.addLayout(hlayout2)



        # vlayout3 = QVBoxLayout(self.page_3)
        # self.progressbar = QProgressBar(self.page_3)
        # self.btn_pause = QPushButton("暂停")
        # self.btn_stop = QPushButton("停止")
        # hlayout3 = QHBoxLayout()
        # hlayout3.addWidget(self.btn_pause)
        # hlayout3.addWidget(self.btn_stop)
        # vlayout3.addWidget(self.progressbar)
        # vlayout3.addLayout(hlayout3)

