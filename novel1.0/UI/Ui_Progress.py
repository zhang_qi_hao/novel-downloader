from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class DProgress(QDialog):
    def __init__(self):
        super(DProgress, self).__init__()
        self.setupUi(self)

    def setupUi(self, DProgress):
        self.centralwidget = DProgress()
        self.centralwidget.setGeometry(600, 400, 300, 40)
        self.centralwidget.setWindowTitle("下载进度")
        self.progress = QProgressBar(self.centralwidget)
        self.progress.setValue(0)
