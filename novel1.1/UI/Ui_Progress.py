import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *


class DProgress(QDialog):
    def __init__(self):
        super(DProgress, self).__init__()
        self.setupUi(self)

    def setupUi(self, DProgress):
        self.centralwidget = DProgress
        self.centralwidget.setGeometry(600, 400, 300, 100)
        self.centralwidget.setWindowTitle("下载进度")
        self.page = QWidget(self.centralwidget)
        self.vlayout = QVBoxLayout()
        self.hlayout = QHBoxLayout()
        self.progress = QProgressBar()
        self.btn_pause = QPushButton("暂停")
        self.btn_cancel = QPushButton("取消")
        self.vlayout.addWidget(self.progress)

        self.hlayout.addWidget(self.btn_pause)
        self.hlayout.addWidget(self.btn_cancel)
        self.vlayout.addLayout(self.hlayout)
        # self.page.setLayout(self.vlayout)
        self.centralwidget.setLayout(self.vlayout)
        self.progress.setValue(0)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = DProgress()
    window.show()
    sys.exit(app.exec_())
