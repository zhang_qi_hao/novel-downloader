import re

import requests




def get_html(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/94.0.4606.71 Safari/537.36'
    }
    try:
        r = requests.get(url, headers=headers)
        r.raise_for_status()
        return r.text
    except:
        return ''


def get_books(surl):
    page = get_html(surl)
    pattern = re.compile(r'<a href="(/book/\d+/)">(\w+)</a>')
    bs = pattern.findall(page)
    books = []
    for item in bs:
        c = surl + item[0]
        books.append({
            'link': c,
            'title': item[1]
        })
   
    return books


def get_chapters(burl):
    page = get_html(burl)
    pattern = re.compile(r'<dd><a href="/book/10150/(\d+\.html)"  >(.*?)</a></dd>')
    cs = pattern.findall(page)
    chapters = []
    for item in cs:
        c = burl + item[0]
        chapters.append({
            'link': c,
            'title': item[1]
        })
    # print(chapters)
    return chapters


def get_content(curl):
    page = get_html(curl)
    pattern = re.compile(r'<div id="content">.*?</div>')
    content = pattern.findall(page)
    print(content)


if __name__ == '__main__':
    # burl = 'https://www.81zw.com/book/10150/'
    surl = 'https://www.81zw.com'
    books = get_books(surl)
    print(books)

   

